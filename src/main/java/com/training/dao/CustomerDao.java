package com.training.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.model.Customer;

public interface CustomerDao extends JpaRepository<Customer,Long> {


}
