package com.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.core.Service;
import com.training.model.Customer;

@RestController
public class CustomerRestController {

    @Autowired
    private Service customerService;


    @GetMapping(value = "/customers", produces = "application/json")
    public List<Customer> customers() {
        return customerService.customers();
    }

    @GetMapping(value = "/customer/{name}", produces = "application/json")
    public Customer customer(@PathVariable String name) {
        return customerService.customer(name);
    }

    @PostMapping(value = "/customer")
    public Customer addCustomer(@RequestParam String name, @RequestParam String address,
                                @RequestParam String phone, @RequestParam String email) {

        return customerService.addCustomer(
            new Customer(name, address, phone, email)
        );
    }
}
