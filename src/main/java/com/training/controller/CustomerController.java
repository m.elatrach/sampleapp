package com.training.controller;

import com.training.core.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomerController {

    @Autowired
    Service customerService;

    @GetMapping("/")
    public String home() {
        return "home";

    }

    @GetMapping("/addCustomer")
    public String addCustomerForm() {

        return "addCustomer";
    }


    @GetMapping("/allCustomers")
    public String allCustomer(Model model) {

        model.addAttribute("allCustomers", customerService.customers());

        return "allCustomers";
    }


}
