package com.training.core;

import com.training.dao.CustomerDao;
import com.training.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class CustomerService implements Service {

    @Autowired
    private CustomerDao customerDao;

    List<Customer> customers = new ArrayList<>();


    public CustomerService() {

    }

    public void initData() {
        customerDao.save(new Customer("Name01", "address01", "01651166", "email1@yopmail.com"));
        customerDao.save(new Customer("Name02", "address02", "01651115", "email2@yopmail.com"));
        customerDao.save(new Customer("Name03", "address03", "01651166", "emai3@yopmail.com"));
        customerDao.save(new Customer("Name04", "address04", "01651164", "email4@yopmail.com"));
    }

    @Override
    public List<Customer> customers() {
        return customerDao.findAll();
//        return getAllCustomers();
    }

    public List<Customer> getAllCustomers() {
        return customers;
    }

    public Customer addCustomer(Customer customer) {
        return customerDao.save(customer);
    }
    public Customer customer(String name) {
       return customers.stream()
           .filter(customer -> customer.getName().equalsIgnoreCase(name))
           .findFirst()
           .orElse(new Customer());

    }
}
