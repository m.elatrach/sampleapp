<%--
  Created by IntelliJ IDEA.
  User: emdc
  Date: 14/03/19
  Time: 08:37 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="text-center">
<div class="cover-container d-flex h-25 p-3 mx-auto flex-column">

<header class="masthead mb-auto">
    <div class="inner">
        <h3 class="masthead-brand">Spring MVC</h3>
        <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link active" href="/app">Home</a>
            <a class="nav-link" href="/app/addCustomer">Add Customer</a>
            <a class="nav-link" href="/app/allCustomers">All Customers</a>
        </nav>
    </div>
</header>
</div>
</div>