<%--
  Created by IntelliJ IDEA.
  User: melatrach
  Date: 12/03/2019
  Time: 14:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Customer</title>
    <%--<link href="bootstrap.min.css" rel="stylesheet" type="text/css">--%>
    <style><%@include file="/WEB-INF/views/bootstrap.min.css"%></style>


</head>
<body>

<jsp:include page="header.jsp" />


<div class="container">

    <div class="py-5 text-center">
        <h2>Add Customer</h2>
        <p class="lead"></p>
    </div>


<form method="post" action="/app/customer">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" placeholder="name" name="name">
    </div>

    <div class="form-group">
        <label for="adress">Adresse</label>
        <textarea class="form-control" id="adress" name="address" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="phone" class="form-control" id="phone" name="phone" placeholder="00000000">
    </div>

    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
    </div>

    <button  class="btn btn-primary" type="submit">Validate</button>

</form>

</div>

<jsp:include page="footer.jsp" />
</body>
</html>
