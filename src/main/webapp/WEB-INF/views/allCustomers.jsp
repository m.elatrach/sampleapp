<%--
  Created by IntelliJ IDEA.
  User: melatrach
  Date: 14/03/2019
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <title>All Customers</title>
    <style><%@include file="/WEB-INF/views/bootstrap.min.css"%></style>

</head>

<body>

    <jsp:include page="header.jsp" />





    <div class="container">

        <div class="py-5 text-center">
            <h2>All Customers</h2>
            <p class="lead"></p>
        </div>


        <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Address</th>
            <th scope="col">Phone</th>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <c:forEach var="customer" items="${allCustomers}">

                    <td>${customer.name}</td>
                    <td>${customer.address}</td>
                    <td>${customer.phone}</td>
                    <td>${customer.email}</td>

            </c:forEach>
        </tr>
        </tbody>
    </table>

</div>


    <jsp:include page="footer.jsp" />

</body>
</html>
