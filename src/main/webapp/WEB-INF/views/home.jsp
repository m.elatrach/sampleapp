<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<title>Training</title>
	<style><%@include file="/WEB-INF/views/bootstrap.min.css"%></style>
</head>

<body>

<jsp:include page="header.jsp" />

	<div class="container" style="margin-top: 200px;margin-bottom: 200px">

		<main class="inner cover text-center">
			<h1 class="cover-heading"></h1>
			<p class="lead"></p>
			<p class="lead">
				<a href="/app/addCustomer" class="btn btn-lg btn-primary">Add Customer</a>
				<a href="/app/allCustomers" class="btn btn-lg btn-primary">All Customers</a>
			</p>
		</main>
	</div>

<jsp:include page="footer.jsp" />
</body>

</html>